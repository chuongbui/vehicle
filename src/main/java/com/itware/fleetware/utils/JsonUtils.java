package com.itware.fleetware.utils;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.itware.fleetware.vehicle.impl.FleetWareException;

/**
 * For serialize, deserialize json
 * 
 * @author Chuong
 *
 */
public class JsonUtils {
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper()
            .configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
            .configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false).setSerializationInclusion(Include.NON_NULL)
            .configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);

    static {
        OBJECT_MAPPER.setVisibility(OBJECT_MAPPER.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(JsonAutoDetect.Visibility.ANY).withGetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
                .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
    }

    private JsonUtils() {
    }

    /**
     * Convert json to a collection
     * 
     * @param json
     * @param collectionType
     * @param elementType
     * @return
     */
    public static <T extends Collection<?>> T toCollection(String json, Class<T> collectionType, Class<?> elementType) {
        try {
            return OBJECT_MAPPER.readValue(json, constructCollectionType(collectionType, elementType));
        } catch (Exception e) {
            throw new FleetWareException(e);
        }
    }

    private static JavaType constructCollectionType(Class<? extends Collection<?>> collectionClass,
            Class<?> elementType) {
        return OBJECT_MAPPER.getTypeFactory().constructCollectionType(collectionClass, elementType);
    }

}
