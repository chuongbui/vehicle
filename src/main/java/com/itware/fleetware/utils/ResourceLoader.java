package com.itware.fleetware.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import com.itware.fleetware.vehicle.impl.FleetWareException;

/**
 * Manipulate resource file
 * 
 * @author Chuong
 *
 */
public class ResourceLoader {
    private ResourceLoader() {
    }

    /**
     * Load all line of resource text file into a string
     * 
     * @param path
     *            path to the resource file
     * @return content of the file as string
     */
    public static String loadResource(String path) {
        try {
            Resource resource = new ClassPathResource(path);
            BufferedReader br = new BufferedReader(new InputStreamReader(resource.getInputStream()), 1024);
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = br.readLine()) != null) {
                stringBuilder.append(line).append('\n');
            }
            br.close();
            return stringBuilder.toString();
        } catch (Exception e) {
            throw new FleetWareException(e);
        }
    }
}
