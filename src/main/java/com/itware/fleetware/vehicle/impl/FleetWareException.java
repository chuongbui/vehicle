package com.itware.fleetware.vehicle.impl;

/**
 * Custom Exception for this project
 * 
 * @author Chuong
 *
 */
public class FleetWareException extends RuntimeException {

    /**
     * Create exception from another one
     * 
     * @param e
     */
    public FleetWareException(Exception e) {
        super(e);
    }

    /**
     * 
     */
    private static final long serialVersionUID = -1188868062269863686L;

}
