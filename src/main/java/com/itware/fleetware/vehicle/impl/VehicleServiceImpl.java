package com.itware.fleetware.vehicle.impl;

import java.util.Collection;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itware.fleetware.vehicle.VehicleEntity;
import com.itware.fleetware.vehicle.VehicleRepository;
import com.itware.fleetware.vehicle.VehicleService;

/**
 * Implement main logic of vehicle
 * 
 * @author Chuong
 *
 */
@Service
public class VehicleServiceImpl implements VehicleService {
    @Autowired
    VehicleRepository vehicleRepository;

    @Override
    public Collection<VehicleEntity> getVehiclesBetween(Date startDate, Date endDate) {
        return vehicleRepository.getVehiclesBetween(startDate, endDate);
    }

}
