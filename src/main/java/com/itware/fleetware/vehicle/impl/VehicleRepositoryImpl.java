package com.itware.fleetware.vehicle.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.itware.fleetware.utils.JsonUtils;
import com.itware.fleetware.utils.ResourceLoader;
import com.itware.fleetware.vehicle.VehicleEntity;
import com.itware.fleetware.vehicle.VehicleRepository;

/**
 * Working with vehicle database
 * 
 * @author Chuong
 *
 */
@Repository
public class VehicleRepositoryImpl implements VehicleRepository {

    @Override
    public Collection<VehicleEntity> getVehiclesBetween(Date startDate, Date endDate) {
        Collection<VehicleEntity> vehicles = getAllVehicle();
        return vehicles.stream().filter(t -> t.between(startDate, endDate)).collect(Collectors.toList());
    }

    /**
     * Just a dummy method, this should be changed to query data from db or any other data source
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    private Collection<VehicleEntity> getAllVehicle() {
        String data = ResourceLoader.loadResource("data.json");
        return JsonUtils.toCollection(data, ArrayList.class, VehicleEntity.class);
    }

}
