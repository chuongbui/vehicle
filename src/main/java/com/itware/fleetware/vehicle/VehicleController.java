package com.itware.fleetware.vehicle;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Handle vehicle REST request
 * 
 * @author Chuong
 *
 */
@RestController
@RequestMapping("/data")
public class VehicleController {
    @Autowired
    private VehicleService vehicleService;

    /**
     * Get vehicles in a time duration
     * 
     * @param requestBody
     * @return
     */
    @RequestMapping(value = "/vehicles", method = RequestMethod.POST)
    public Collection<VehicleEntity> vehicle(@RequestBody VehicleRequest requestBody) {
        return vehicleService.getVehiclesBetween(requestBody.getStartDate(), requestBody.getEndDate());
    }
}
