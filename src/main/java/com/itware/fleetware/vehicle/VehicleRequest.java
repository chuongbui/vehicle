package com.itware.fleetware.vehicle;

import java.util.Date;

/**
 * Store POST data of get vehicles request
 * 
 * @author Chuong
 *
 */
public class VehicleRequest {
    private Date startDate;
    private Date endDate;

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
}
