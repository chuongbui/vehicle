package com.itware.fleetware.vehicle;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * Contains information of Vehicle at a specific time
 * 
 * @author Chuong
 *
 */
public class VehicleEntity {

    private String vehicleId;

    private String uniqueId;

    private String licensePlate;

    private Date executionDate;

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private double objectLongitude;
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private double objectLatitude;

    public String getVehicleId() {
        return vehicleId;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public Date getExecutionDate() {
        return executionDate;
    }

    public double getObjectLongitude() {
        return objectLongitude;
    }

    public double getObjectLatitude() {
        return objectLatitude;
    }

    /**
     * Check if vehicle is between a time duration
     * 
     * @param startDate
     * @param endDate
     * @return
     */
    public boolean between(Date startDate, Date endDate) {
        if (executionDate == null) {
            return false;
        }
        boolean result = true;
        if (startDate != null) {
            result = !executionDate.before(startDate);
        }
        if (endDate != null) {
            result = result && !executionDate.after(endDate);
        }
        return result;
    }
}
