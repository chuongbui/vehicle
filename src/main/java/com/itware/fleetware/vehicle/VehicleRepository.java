package com.itware.fleetware.vehicle;

import java.util.Collection;
import java.util.Date;

/**
 * Interface for vehicle repository
 * 
 * @author Chuong
 *
 */
@FunctionalInterface
public interface VehicleRepository {
    /**
     * Get vehicles that operate in a limited time
     * 
     * @param startDate
     *            from this time
     * @param endDate
     *            to this time
     * @return
     */
    Collection<VehicleEntity> getVehiclesBetween(Date startDate, Date endDate);
}
