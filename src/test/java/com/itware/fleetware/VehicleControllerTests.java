package com.itware.fleetware;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/**
 * @author Chuong
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
@WebAppConfiguration
public class VehicleControllerTests {

    @Autowired
    private WebApplicationContext ctx;

    private MockMvc mockMvc;

    @Before
    public void setUp() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(ctx).build();
    }

    @Test
    public void testGetVehicleAllData() throws Exception {
        String requestJson = "{" + "\"startDate\": \"2014-08-08T10:30:28+0200\","
                + "\"endDate\": \"2017-08-10T10:30:28+0200\"" + "}";
        this.mockMvc
                .perform(post("/data/vehicles").content(requestJson).contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()).andExpect(jsonPath("$[0].vehicleId").value("100001087"))
                .andExpect(jsonPath("$[0].uniqueId").value("Renault Master 2016"))
                .andExpect(jsonPath("$[0].licensePlate").value("NRU-685"))
                .andExpect(jsonPath("$[0].executionDate").value("2016-08-02T10:26:48+0200"))
                .andExpect(jsonPath("$[0].objectLongitude").value("8.2222668"))
                .andExpect(jsonPath("$[0].objectLatitude").value("48.8666835999"))
                .andExpect(jsonPath("$[5].vehicleId").value("100000175"))
                .andExpect(jsonPath("$[5].uniqueId").value("Audi A8"))
                .andExpect(jsonPath("$[5].licensePlate").value("NRU-683"))
                .andExpect(jsonPath("$[5].executionDate").value("2015-08-01T14:35:10+0200"))
                .andExpect(jsonPath("$[5].objectLongitude").value("19.6874886"))
                .andExpect(jsonPath("$[5].objectLatitude").value("46.8760015999"));
    }

    @Test
    public void testGetVehicleNoData() throws Exception {
        String requestJson = "{" + "\"startDate\": \"2012-08-08T10:30:28+0200\","
                + "\"endDate\": \"2012-08-10T10:30:28+0200\"" + "}";
        this.mockMvc
                .perform(post("/data/vehicles").content(requestJson).contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()).andExpect(MockMvcResultMatchers.jsonPath("$").isEmpty());
    }

    @Test
    public void testGetVehicleEmptyStartDate() throws Exception {
        String requestJson = "{" + "\"endDate\": \"2016-08-10T10:30:28+0200\"" + "}";
        this.mockMvc
                .perform(post("/data/vehicles").content(requestJson).contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()).andExpect(jsonPath("$[0].vehicleId").value("100001087"))
                .andExpect(jsonPath("$[0].uniqueId").value("Renault Master 2016"))
                .andExpect(jsonPath("$[0].licensePlate").value("NRU-685"))
                .andExpect(jsonPath("$[0].executionDate").value("2016-08-02T10:26:48+0200"))
                .andExpect(jsonPath("$[0].objectLongitude").value("8.2222668"))
                .andExpect(jsonPath("$[0].objectLatitude").value("48.8666835999"));
    }

    @Test
    public void testGetVehicleEmptyEndDate() throws Exception {
        String requestJson = "{" + "\"startDate\": \"2016-08-02T10:26:56+0200\"" + "}";
        this.mockMvc
                .perform(post("/data/vehicles").content(requestJson).contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()).andExpect(jsonPath("$[0].vehicleId").value("100001087"))
                .andExpect(jsonPath("$[0].uniqueId").value("Renault Master 2016"))
                .andExpect(jsonPath("$[0].licensePlate").value("NRU-685"))
                .andExpect(jsonPath("$[0].executionDate").value("2016-08-02T10:26:57+0200"))
                .andExpect(jsonPath("$[0].objectLongitude").value("8.2223991"))
                .andExpect(jsonPath("$[0].objectLatitude").value("48.8667381"));
    }

    @Test
    public void testGetVehicleEmptyStartEndDate() throws Exception {
        String requestJson = "{}";
        this.mockMvc
                .perform(post("/data/vehicles").content(requestJson).contentType(MediaType.APPLICATION_JSON)
                        .accept(MediaType.APPLICATION_JSON))
                .andDo(print()).andExpect(status().isOk()).andExpect(jsonPath("$[0].vehicleId").value("100001087"))
                .andExpect(jsonPath("$[0].uniqueId").value("Renault Master 2016"))
                .andExpect(jsonPath("$[0].licensePlate").value("NRU-685"))
                .andExpect(jsonPath("$[0].executionDate").value("2016-08-02T10:26:48+0200"))
                .andExpect(jsonPath("$[0].objectLongitude").value("8.2222668"))
                .andExpect(jsonPath("$[0].objectLatitude").value("48.8666835999"))
                .andExpect(jsonPath("$[5].vehicleId").value("100000175"))
                .andExpect(jsonPath("$[5].uniqueId").value("Audi A8"))
                .andExpect(jsonPath("$[5].licensePlate").value("NRU-683"))
                .andExpect(jsonPath("$[5].executionDate").value("2015-08-01T14:35:10+0200"))
                .andExpect(jsonPath("$[5].objectLongitude").value("19.6874886"))
                .andExpect(jsonPath("$[5].objectLatitude").value("46.8760015999"));
    }

}
